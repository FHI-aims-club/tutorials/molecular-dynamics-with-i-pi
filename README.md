# Molecular Dynamics with i-PI


The contents of this tutorial are available at:

[https://fhi-aims-club.gitlab.io/tutorials/molecular-dynamics-with-i-pi](https://fhi-aims-club.gitlab.io/tutorials/molecular-dynamics-with-i-pi)

## How to locally setup the website

1. Clone the project:
    ```
    git clone git@gitlab.com:FHI-aims-club/tutorials/molecular-dynamics-with-i-pi.git
    ```
2. Change into the folder `molecular-dynamics-with-i-pi` and install the requirements:
    ```
    pip install -r requirements.txt
    ```
3. Run:
   ```
    mkdocs serve --strict
   ```

Mkdocs now builds the website and makes it available under `localhost:8000` (you can just enter this in your browser). The port might be a different on your platform, but mkdocs tells you where to find the website:

```
Serving on http://127.0.0.1:8000/tutorials/molecular-dynamics-with-i-pi/
```

Mkdocs now watches your changes and rebuilds whenever you change something in the files (you need to save your changes). So you can watch your changes live in the browser. 

## Publish your changes

After finalizing your changes, please don't forget to add, commit and push to the remote repo:

1. Add all (if applicable):
    ```
    git add <filename>
    ```
2. Commit and describe your changes:
    ```
    git commit -m "My contributions"
    ```
3. Pull first:
    ```
    git pull
    ```
4. Push now:
    ```
    git push
    ```

The gitlab CI/CD pipeline will now build the website and deploy it to the following page:

[https://fhi-aims-club.gitlab.io/tutorials/molecular-dynamics-with-i-pi](https://fhi-aims-club.gitlab.io/tutorials/molecular-dynamics-with-i-pi)
