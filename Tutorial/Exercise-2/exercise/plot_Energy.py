# !/usr/env/bin python

from matplotlib import pyplot as plt
import numpy as np
import sys

filenames = sys.argv[1:4]

for filename in filenames:
    print(filename)

    data = np.loadtxt(filename, usecols=(1,2))
    if 'extreme' in filename:
        plt.plot(data[:,0]*1000, data[:,1], 'b:', label=str(filename.split('/')[0]))
    elif 'default' in filename:
        plt.plot(data[:,0]*1000, data[:,1], 'orange', linestyle='-', label=str(filename.split('/')[0]))
    elif 'loose' in filename:
        plt.plot(data[:,0]*1000, data[:,1], 'r--', label=str(filename.split('/')[0]))


# Format 
plt.xlabel(r'time / fs',fontsize=20)
plt.ylabel(r'conserved energy (Ha)',fontsize=20)
plt.xlim([0.,150.])
plt.legend(loc='best',fontsize=15)
plt.xticks(fontsize = 13)
plt.yticks(fontsize = 13)
plt.tight_layout()
plt.savefig('%s-%s-%s-energy-drift.png'%(filenames[0].split('/')[0],filenames[1].split('/')[0],filenames[2].split('/')[0]), dpi=300)


plt.show()


