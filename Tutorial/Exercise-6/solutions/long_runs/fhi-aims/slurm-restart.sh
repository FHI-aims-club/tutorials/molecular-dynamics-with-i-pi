#!/bin/bash -l
## NOTE the -l flag!

## do not join stdout and stderr
#SBATCH -o job.%j.out
#SBATCH -e job.%j.err

## name of the job
#SBATCH -J ipi-T.I.

## execute job from the current working directory
#SBATCH -D ./
## do not send mail
#SBATCH --mail-type=NONE

## 72 on Raven, 40 on cobra.
#SBATCH --ntasks-per-node=40
#SBATCH --nodes=2

#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-core=1

#SBATCH --partition=general

## run time hh:mm:ss
#SBATCH -t 05:10:00

#module purge
#module load intel/21.2.0
#module load impi/2021.2
#module load mkl/2021.2
#module load anaconda/3

export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MKL_HOME/lib/intel64:$INTEL_HOME/compiler/latest/linux/compiler/lib/intel64_lin/

set -e

AIMS_EXE='/u/seko/software/FHIaims2021_210716/build/b869c87_cmake.O3/aims.210716_2.scalapack.mpi.x'
IPI_EXE=i-pi
#. /u/kfidan/soft/i-pi-sabia/env.sh

date

NTIME=18000  # 10 hours

rootdir=`pwd`

HOST=`echo $HOSTNAME`
echo "IPI NODE ADDRESS IS $HOST"

echo  {"init",$( date )} >> $rootdir/LIST

################  i-PI  ################
iport=0
for dir in */
do
  cd $dir
    if [ -f STOP ];  then cd ..; continue; fi

    if [ -f RESTART ]
    then
      step=`grep '^\s*<step>' RESTART | grep -o '[0-9]*'`
      total_steps=`grep '^\s*<total_steps>' RESTART|grep -o '[0-9]*'`
      if [ $step -ge $total_steps ]
      then
        # Check for finish and put STOP if reached total_steps (here and after runs)
        touch STOP
        echo "$dir reached total number of steps." >> $rootdir/LIST
        cd ..
        continue
      fi

      grep '<step>' RESTART >> $rootdir/LIST
      cp -p RESTART RESTART.save
    fi

    if [ -f EXIT ]
    then
      rm EXIT
    fi

    sed -e "s:<address>.*:<address>${HOST}</address>:" RESTART > INITIAL.xml
    # --- Only for many folders in the same run ---
    sed -i -e "s:<port>.*</port>:<port> $((31415 + iport)) </port>:" INITIAL.xml
    iport=$((iport + 1))
    # ---------------------------------------------
    sed -i -e "s:<total_time>.*</total_time>:<total_time> $NTIME </total_time>:" INITIAL.xml
    sed -i -e "s/localhost/${HOST}/g" INITIAL.xml

    echo "Launching i-PI in $dir ..."
    # -u disables buffering of output in python
    ${IPI_EXE} INITIAL.xml > log.ipi &
  cd ..
done

sleep 20  # I launch all i-pi instances first in order to sleep only once after all of them

################  AIMS  ################ 
iport=0
for dir in */
do
  cd $dir
    if [ -f STOP ];  then cd ..; continue; fi

    cd aims
      sed -e "s/localhost/${HOST}/g" -e "s/portnumber/$((31415 + iport))/g" ../template-control.in > control.in
      iport=$((iport + 1))
      echo "Launching aims in ${dir} ..."
      srun  --ntasks=6 \
            --cpus-per-task=1 \
            --ntasks-per-core=1 \
            --mem=5000 \
            --exclusive \
            ${AIMS_EXE} > aims.out &
      sleep 10
    cd ..
  cd ..
done

############ WAIT FOR ALL #############
wait
sleep 30    # Maybe some buffers need time to flush
date

############## SAVE files ##############
echo  {"End",$( date )} >> $rootdir/LIST
echo '1' >> $rootdir/count
l=`cat count|wc -l`

for dir in */
do
  cd $dir
    if [ -f RESTART ]
    then
        echo "grep RESTART in $dir ..." >&2  # to see which one doesn't exist.
        grep '<step>' RESTART |awk -v d=$dir "{print d, \$0}" >> $rootdir/LIST

        # Check for finish and put STOP if reached total_steps (here and before runs)
        step=`grep '^\s*<step>' RESTART | grep -o '[0-9]*'`
        total_steps=`grep '^\s*<total_steps>' RESTART|grep -o '[0-9]*'`
        if [ $step -ge $total_steps ]
        then
          touch STOP
        fi
    fi

    cd aims
        if [ ! -d OUTPUT ]
        then
            mkdir OUTPUT
        fi
        cp -p aims.out OUTPUT/aims_$l.out
    cd ..

    if [ ! -d IPI_LOGS ]
    then
        mkdir IPI_LOGS
    fi
    cp -p log.ipi IPI_LOGS/log.ipi_$l
  cd ..
done

################ Resubmit ################
nstops=`ls -l */STOP | wc -l`
echo "nSTOPs = $nstops" >> $rootdir/LIST
if [ $nstops -ge 11 ]
then
  touch STOP
fi

if [ ! -f "STOP" ]
then
    echo "RESTART: YES." >> $rootdir/LIST
    sbatch slurm-restart.sh
else
  echo "Stop file is found." >> $rootdir/LIST
fi
