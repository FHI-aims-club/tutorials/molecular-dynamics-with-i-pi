#!/bin/bash

# Required simulation time is in fact much longer than what can be done
# in a single run. So, this simple script won't do the job and provided
# for simplicity. For the example of a real production script
# for MPCDF Garching machines, see scripts/slurm-example.sh.

. /path-to/i-pi/env.sh
ipi_command="/path-to/i-pi/bin/i-pi"
aims_command="mpirun -np 4 /path-to/fhi-aims/build/aims.200716_2.mpi.x"

for xx in 10 20 40 60 80 100 120 140 160 180 200
do
  cd ${xx}
    echo 'Running : ' ${xx} 
    # python -u flag disables buffering of outputs
    # and allows you to see the messages in real time.
    # Very helpful for debugging purposes.
    python3 -u ${ipi_command} input.xml |tee log.ipi &
    sleep 3
    ${aims_command} > aims.out &
    wait
  cd ..
done
