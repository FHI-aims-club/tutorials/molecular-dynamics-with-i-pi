#!/bin/bash -l
## NOTE the -l flag!

## do not join stdout and stderr
#SBATCH -o job.%j.out
#SBATCH -e job.%j.err

## name of the job
#SBATCH -J ipi-T.I.

## execute job from the current working directory
#SBATCH -D ./
## do not send mail
#SBATCH --mail-type=NONE

## 72 cores per node on Raven, 40 on Cobra.
#SBATCH --ntasks-per-node=72
#SBATCH --nodes=1
#SBATCH --cpus-per-task=1

#SBATCH --partition=general

## run time hh:mm:ss
#SBATCH -t 10:20:00

module purge
module load intel/21.2.0
module load impi/2021.2
module load mkl/2021.2
module load anaconda/3

export OMP_NUM_THREADS=1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MKL_HOME/lib/intel64:$INTEL_HOME/compiler/latest/linux/compiler/lib/intel64_lin/

set -e

AIMS_EXE='/u/user/bin/aims.210716_2.scalapack.mpi.x'
IPI_EXE=/u/user/bin/i-pi
. /u/user/soft/i-pi/env.sh

date

NTIME=36000  # 10 hours

rootdir=`pwd`

HOST=`echo $HOSTNAME`
echo "IPI NODE ADDRESS IS $HOST"

echo  {"init",$( date )} >> $rootdir/LIST

################  i-PI  ################
iport=0
for dir in */
do
  cd $dir
    if [ -f RESTART ]
    then
      grep '<step>'  RESTART >> $rootdir/LIST
      cp RESTART RESTART.save
    fi

    if [ -f EXIT ]
    then
      rm EXIT
    fi

    # You should substitute input.template.xml with RESTART in subsequent runs
    sed -e "s:<address>.*:<address>${HOST}</address>:" input.template.xml > INITIAL.xml

    # ===== Changing ports needed if running many i-pi instances on the same node =====
    sed -i -e "s:<port>.*</port>:<port> $((31415 + iport)) </port>:" INITIAL.xml
    iport=$((iport + 1))
    # =================================================================================

    # Telling i-pi maximal allowed run time.
    sed -i -e "s:<total_time>.*</total_time>:<total_time> $NTIME </total_time>:" INITIAL.xml
    # Telling i-pi the address of the node where it resides
    sed -i -e "s/localhost/${HOST}/g" INITIAL.xml

    echo "Launching i-PI in $dir ..."
    # -u disables buffering of output in python
    python3 -u ${IPI_EXE} INITIAL.xml > log.ipi &
  cd ..
done

sleep 20  # We launch all i-pi instances first in order to sleep only once after all of them.
          # We need to sleep because i-pi needs time to initialize before Aims tries to connect.
          # Otherwise "connection refused" error pops up.

################  AIMS  ################ 
iport=0
for dir in */
do
  cd $dir
    cd aims
      # Passing address and port of i-pi into control.in
      sed -e "s/localhost/${HOST}/g" -e "s/portnumber/$((31415 + iport))/g" ../template-control.in > control.in
      iport=$((iport + 1))
      echo "Launching aims in ${dir} ..."
      # In this particular example, tasks are small and we launch many of them on the same node,
      # therefore we need --mem and --overlap. Otherwise it's not needed.
      srun -n 6 --mem=10000 --overlap ${AIMS_EXE} > aims.out &
      sleep 10
    cd ..
  cd ..
done

############ WAIT FOR ALL #############
wait
sleep 30    # Maybe some buffers need time to flush
date

############## SAVE files ##############
echo  {"End",$( date )} >> $rootdir/LIST
echo '1' >> $rootdir/count
l=`cat count|wc -l`

for dir in */
do
  cd $dir
    grep '<step>' RESTART |awk -v d=$dir "{print d, \$0}" >> $rootdir/LIST

    cd aims
        if [ ! -d OUTPUT ]
        then
            mkdir OUTPUT
        fi
        cp aims.out OUTPUT/aims_$l.out
    cd ..

    if [ ! -d IPI_LOGS ]
    then
        mkdir IPI_LOGS
    fi
    cp log.ipi IPI_LOGS/log.ipi_$l
  cd ..
done
