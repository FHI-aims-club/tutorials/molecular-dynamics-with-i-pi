#!/usr/bin/env python3

import numpy as np
import sys
np.set_printoptions(precision=3, suppress=True)

kb_ev = 8.6173303e-5  # eV/K

def intgrt(x, y):
    I = []
    for i in range(0, len(x)):
        I.append(np.trapz(y[: i + 1], x[: i + 1]))
    I = np.array(I)
    return I


# Check syntax
if len(sys.argv) != 3:
    print("")
    print("Error: wrong number of files.")
    print("This script takes exactly 1 input and 1 output file.\n")
    sys.exit(1)
else:
    input_file = sys.argv[1]
    output_file = sys.argv[2]

# Reading data
try:
    raw_data = np.loadtxt(input_file)
except:
    print("We can't find the file '{}'.".format(input_file))
    print("")
    sys.exit()
T = raw_data[:, 0]
#beta = 1 / (kb_ev * T)
U = raw_data[:, 1]
U_err = raw_data[:, 2]
# Instead of integrating by dbeta, we integrate by dT,
# therefore we need to change variables correctly.
U_jacobian = U / (T*T)
U_err_jacobian = U_err / (T*T)

# Integrating data
#integral = intgrt(beta, U)
#integral_err = intgrt(beta, U_err)
integral = intgrt(T, U_jacobian)
integral_err = intgrt(T, U_err_jacobian)

# Output
data = np.stack((T, integral, integral_err), axis=1)
np.savetxt(output_file,
           data,
           fmt=("%3d", "%e", "%e"),
           header="T (K)   integral (eV)   integral_err (eV)",
          )
np.savetxt(sys.stdout.buffer,
           data,
           fmt=("%3d", "%.6f", "%.6f"),
           header="T (K)   integral (eV)   integral_err (eV)",
          )
