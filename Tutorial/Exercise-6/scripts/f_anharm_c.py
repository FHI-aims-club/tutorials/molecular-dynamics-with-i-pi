#!/usr/bin/env python3

import numpy as np
import sys

kb_ev = 8.6173303e-5  # eV/K

E_total = 0.0  # Potential of the relaxed molecule. We omit it for simplicity.

# Check syntax
if len(sys.argv) != 4:
    print("")
    print("Error: wrong number of arguments.")
    print("This script takes exactly 2 input and 1 output file.")
    print()
    sys.exit(1)
else:
    f_harm = sys.argv[1]
    u_integrated = sys.argv[2]
    output_file = sys.argv[3]

# Reading harmonic free energy
try:
    data_harm = np.loadtxt(f_harm)
except:
    print("We can't find the file '{}'.\n".format(f_harm))
    sys.exit(1)
T = data_harm[:, 0]
F_h = data_harm[:, 1]

# Reading integrated anharmonic potential energy
try:
    # Assumed input format is "T integral integral_err"
    data_u_int = np.loadtxt(u_integrated)
except:
    print("We can't find the file '{}'.\n".format(u_integrated))
    sys.exit(1)
integral = data_u_int[:, 1]
integral_err = data_u_int[:, 2]

# Calculating anharmonic free energy
F_anh = (E_total + F_h - T * integral)  # F2_anh = F2_harm_c - T * int (<Uanh>/T^2 dT) from T1 to T2
F_anh_err = T * integral_err

# Output
data = np.stack((T, F_anh, F_anh_err), axis=1)
np.savetxt(
    output_file,
    data,
    fmt=("%3d", "%.6f", "%.6f"),
    header="T (K)   F_anh (eV)  F_anh_err (eV)",
)
np.savetxt(
    sys.stdout.buffer,
    data,
    fmt=("%3d", "%12.4f", "%10.4f"),
    header="T (K)   F_anh (eV)  F_anh_err (eV)",
)
