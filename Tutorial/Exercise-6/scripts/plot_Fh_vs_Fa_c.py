#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

font = {"family": "sans-serif", "weight": "normal", "size": 18}
matplotlib.rc("font", **font)

# Check syntax
if len(sys.argv) != 3:
    print("Error: wrong number of input files.")
    print(
        "This script takes exactly 2 input files:"
        "\n\tf_harm_c\n\tf_anharm_c\n"
    )
    sys.exit(1)
else:
    f_harm_c_file   = sys.argv[1]
    f_anharm_c_file = sys.argv[2]

# Reading and plotting classical harmonic free energy
try:
    # Expected data: "T  Fh"
    data_harm_c = np.loadtxt(f_harm_c_file)
except:
    print("We cannot open the file '{}'.\n".format(f_harm_c_file))
    sys.exit(1)

assert data_harm_c.shape[1] == 2, "Wrong dimensions in the file '%s'" % f_harm_c_file
T  = data_harm_c[:, 0]
Fh = data_harm_c[:, 1]
plt.plot(T, Fh, "--g", label="$F_h^c$")

# Reading and plotting classical anharmonic free energy
try:
    # Expected data: "T  Fan  Fan_err"
    data_anh_c = np.loadtxt(f_anharm_c_file)
except:
    print("We cannot open the file '{}'.\n".format(f_anharm_c_file))
    sys.exit(1)

assert data_anh_c.shape[1] == 3, "Wrong dimensions in the file '%s'" % f_anharm_c_file
T       = data_anh_c[:, 0]
Fan     = data_anh_c[:, 1]
Fan_err = data_anh_c[:, 2]
plt.errorbar(T, Fan, Fan_err, color="g", label="$F_{anh}^c$")

plt.legend()
plt.xlim(9, 201)
plt.ylim(0, 1.25)
plt.xlabel("T (K)")
plt.ylabel("F (eV)")
plt.show()
