#!/usr/bin/env python3

import numpy as np
import sys


# Check syntax
if len(sys.argv) != 5:
    print("")
    print("Error: wrong number of arguments.")
    print("This script takes exactly 3 input and 1 output file.")
    print()
    sys.exit(1)
else:
    f_harm_c = sys.argv[1]
    f_harm_q = sys.argv[2]
    f_anharm_c = sys.argv[3]
    output_file = sys.argv[4]

# Reading classical harmonic free energy
try:
    data_harm_c = np.loadtxt(f_harm_c)
except:
    print("We can't find the file '{}'.\n".format(f_harm_c))
    sys.exit(1)
T = data_harm_c[:, 0]
F_hc = data_harm_c[:, 1]

# Reading quantum harmonic free energy
try:
    data_harm_q = np.loadtxt(f_harm_q)
except:
    print("Cannot find the file '{}'.\n".format(f_harm_q))
    sys.exit(1)
T = data_harm_q[:, 0]
F_hq = data_harm_q[:, 1]

# Reading classical anharmonic free energy
try:
    data_anharm_c = np.loadtxt(f_anharm_c)
except:
    print("Cannot find the file '{}'.\n".format(f_anharm_c))
    sys.exit(1)
T = data_anharm_c[:, 0]
F_anh_c = data_anharm_c[:, 1]
F_anh_c_err = data_anharm_c[:, 2]

# Calculating anharmonic quantum free energy
assert len(F_anh_c) == len(F_hc) == len(F_hq), "Data size mismatch, check inputs."
F_anh_q = F_anh_c - F_hc + F_hq

data = np.stack((T, F_anh_q, F_anh_c_err), axis=1)
np.savetxt(output_file, data, fmt=("%3d", "%10.4f", "%10.4f"), header="T    F_anh_q (eV)   F_anh_q_err (eV)")
np.savetxt(sys.stdout.buffer, data, fmt=("%3d", "%10.4f", "%10.4f"), header="T    F_anh_q (eV)   F_anh_q_err (eV)")
