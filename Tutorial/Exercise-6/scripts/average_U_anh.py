#!/usr/bin/env python3

import numpy as np
import os
import sys
from scipy import signal
#import matplotlib.pyplot as plt

kb_ev = 8.6173303e-5  # eV/K
natoms = 7  # we work with H5O2+ cation
# We put the energy baseline here for simplicity, but in principle
# it's just the potential of a relaxed molecule.
E_tot = -4140.222346595  # eV (default Light settings, forces relaxed to 1E-3 eV/Ang)

def error_from_u(data_raw):
    N = len(data_raw)
    data = data_raw - np.mean(data_raw)
    data = data.flatten()
    stdev = np.std(data)

    # Mode "full" produces symmetrical ACF twice the size of the original array,
    # we take only its second half.
    acf = signal.correlate(data, data, mode="full")[data.size :]
    # We normalize the ACF so that ACF(t=0) = 1
    acf /= acf[0]

    # You can uncomment this and 'import matplotlib' to plot the ACF
    # plt.plot(acf)
    # plt.show()

    # The first place where the ACF is below the threshold of 0.02
    tau = np.where(acf < 0.02)[0][0]
    N_independent = N / tau

    return stdev / np.sqrt(N_independent)


def ipi_md_potential(fpath, bexclude=1000):
    subfolders = []
    for f in os.listdir(fpath):
        if os.path.isdir(os.path.join(fpath, f)):
            subfolders.append(f)
    # We want the folder names being sorted by temperature, not alphabetically
    subfolders.sort(key=float)
    Uanh = []
    err = []
    for subfolder in subfolders:
        infile = os.path.join(fpath, subfolder, "simulation.out")
        try:
            # column 4 -> potential{electronvolt}: the physical system's potential energy
            # column 5 -> kinetic_md{electronvolt}: the kinetic energy of the classical system
            rawdata = np.loadtxt(infile, usecols=(3, 4), skiprows=bexclude)
        except:
            print("Error: cannot load %s" % infile)
            sys.exit(1)

        U = rawdata.sum(axis=1)  # U = E_pot + E_kin
        # We should either subtract the baseline here or add it to the Uh
        Um = np.mean(U) - E_tot
        err.append(error_from_u(U))
        beta = 1 / (kb_ev * float(subfolder))
        # 3N-3 because in i-PI we can constrain center of mass, but cannot constrain rotations.
        Uh = (3*natoms - 6 + 1.5) / beta  # 3/2 kT for Epot + 3/2 kT for E_kin, and rotations have only E_kin
        Uanh.append(Um - Uh)

    return (
        np.array(subfolders, dtype=float),  # T
        np.array(Uanh, dtype=float),
        np.array(err, dtype=float),
    )


if __name__ == "__main__":
    # Check syntax
    if len(sys.argv) == 3:
        input_folder = sys.argv[1]
        output_file = sys.argv[2]
        if not os.path.isdir(input_folder):
            print("the provided path %s" % input_folder)
            print("is not a directory.")
            sys.exit(1)
    else:
        print("")
        print("Error: wrong arguments.")
        print("This script takes one directory for input and one filename for output.\n")
        sys.exit(1)

    # Calculate anhramonic part of potential energy
    (T, Uanh, U_err) = ipi_md_potential(input_folder)

    # Output
    outdata = np.stack((T, Uanh, U_err), axis=1)
    np.savetxt(
        output_file,
        outdata,
        fmt=("%3i", "%10.6f", "%10.6f"),
        header="T [K]    <U_anh> (eV)     U_err (eV)",
    )
    np.savetxt(
        sys.stdout.buffer,
        outdata,
        fmt=("%3i", "%10.6f", "%10.6f"),
        header="T [K]    <U_anh> (eV)     U_err (eV)",
    )
