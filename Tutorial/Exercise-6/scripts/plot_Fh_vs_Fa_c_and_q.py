#!/usr/bin/env python3

import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

font = {"family": "sans-serif", "weight": "normal", "size": 16}
matplotlib.rc("font", **font)

# Check syntax
if len(sys.argv) != 5:
    print("Error: wrong number of input files.")
    print(
        "This script takes exactly 4 input files:"
        "\n\tf_harm_c\n\tf_anharm_c"
        "\n\tf_harm_q\n\tf_anharm_q\n"
    )
    sys.exit(1)
else:
    f_harm_c_file   = sys.argv[1]
    f_anharm_c_file = sys.argv[2]
    f_harm_q_file   = sys.argv[3]
    f_anharm_q_file = sys.argv[4]

# Reading and plotting quantum anharmonic free energy
try:
    # Expected data: "T  Fan_q  Fan_q_err"
    data_anh_q = np.loadtxt(f_anharm_q_file)
except Exception as e:
    print(e)
    print("We cannot open the file '{}'.\n".format(f_anharm_q_file))
    sys.exit(1)

assert data_anh_q.shape[1] == 3, "Wrong dimensions in the file '%s'" % f_anharm_q_file
T         = data_anh_q[:, 0]
Fan_q     = data_anh_q[:, 1]
Fan_q_err = data_anh_q[:, 2]
plt.errorbar(T, Fan_q, Fan_q_err, color="b", label="$F_{anh}^q$")

# Reading and plotting quantum harmonic free energy
try:
    data_harm_q = np.loadtxt(f_harm_q_file)
except:
    print("We cannot open the file '{}'.\n".format(f_harm_q_file))
    sys.exit(1)

assert data_harm_q.shape[1] == 2, "Wrong dimensions in the file '%s'" % f_harm_q_file
T = data_harm_q[:, 0]
Fh = data_harm_q[:, 1]
plt.plot(T, Fh, "--b", label="$F_h^q$")

# Reading and plotting classical anharmonic free energy
try:
    # Expected data: "T  Fan  Fan_err"
    data_anh_c = np.loadtxt(f_anharm_c_file)
except:
    print("We cannot open the file '{}'.\n".format(f_anharm_c_file))
    sys.exit(1)

assert data_anh_c.shape[1] == 3, "Wrong dimensions in the file '%s'" % f_anharm_c_file
T       = data_anh_c[:, 0]
Fan     = data_anh_c[:, 1]
Fan_err = data_anh_c[:, 2]
plt.errorbar(T, Fan, Fan_err, color="g", label="$F_{anh}^c$")

# Reading and plotting classical harmonic free energy
try:
    data_harm_c = np.loadtxt(f_harm_c_file)
except:
    print("We cannot open the file '{}'.\n".format(f_harm_c_file))
    sys.exit(1)

assert data_harm_c.shape[1] == 2, "Wrong dimensions in the file '%s'" % f_harm_c_file 
T  = data_harm_c[:, 0]
Fh = data_harm_c[:, 1]
plt.plot(T, Fh, "--g", label="$F_h^c$")

plt.legend()
plt.grid()
plt.xlim(9, 201)
plt.ylim(0, 2)
plt.xlabel("T (K)")
plt.ylabel("F (eV)")
plt.show()
