# Molecular Dynamics: a client/server approach

*Timing: $\sim$ 20 minutes total*

In this [exercise](https://gitlab.com/FHI-aims-club/tutorials/molecular-dynamics-with-i-pi/-/tree/main/Tutorial/Exercise-1/exercise/) we are going to familiarize ourselves with the syntax of i-PI input files. As mentioned previously, the i-PI program works through a client-server architecture. It can use INTERNET or UNIX sockets, that allow the system to be simulated on the same machine or on a different machine, as long as the calculation can communicate with the server.

A schematic representation of the communication between FHI-aims and i-PI is shown in the figure below. Once the connection is established, the simulation loop follows these steps: i) i-PI sends the nuclear positions and cell parameters to FHI-aims; ii) FHI-aims calculates the required properties, such as energies and forces, and returns the data; iii) i-PI then updates the atomic positions using the received information, restarting the loop. The red and blue arrows indicate the flow of data within and between codes, respectively.

<center>
    <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-1/i-pi-client-structure.png" title="i-PI communication channels" alt="i-PI communication channels" height="87%" width="87%">
<figcaption>Diagram of the FHI-aims and i-PI communication</figcaption>
</center>

Let's now take a look at the input files. The simulation example here will consist of one Zundel cation in the canonical (NVT) ensemble at 300 K.


## **Server**

An example of an input file for i-PI can be found in [`input.xml`](/tutorials/molecular-dynamics-with-i-pi/Exercise-1/exercise/input.xml). It is an xml file, which is quite intuitive to learn.
Please take your time to understand the keywords that are there and consult
the [i-PI manual](https://ipi-code.org/i-pi/).
We will be using UNIX sockets here, which is the most convenient way to use the code
when running both servers and clients in small desktop (or laptop) machines.
For that, we have to specify only the `<address>`, *which can simply be a string
containing a name of your choice*. For internet sockets, one would have to provide
the relevant IP address for the `<address>` field and a number for the `<port>` field.

## **Client**

The keyword to add to the [`control.in`](/tutorials/molecular-dynamics-with-i-pi/Exercise-1/exercise/control.in) file of FHI-aims is

    use_pimd_wrapper UNIX:<address> <portnumber>

where `<address>` should be substituted by the name of the socket you choose, and the port can be any number since it does not play a role for UNIX sockets. 
The address that goes in the `control.in` file should match the one in the `input.xml` file of i-PI. The [`geometry.in`](/tutorials/molecular-dynamics-with-i-pi/Exercise-1/exercise/geometry.in) is only provided for the initialization of FHI-aims, and in this mode FHI-aims does not use the atomic positions in this file (except to initialize numerical quantities). However, it is very important to note that:

!!! danger "Matching species order in `geometry.in` and i-PI input"
     In order to avoid inconsistencies, the order of atomic species in the `geometry.in` file of FHI-aims and the file containing the initial geometry read by i-PI (normally something like `init.xyz`) must match.

??? example "Note"
    In all subsequent exercises you will be free to choose `<address>` as you want, and you should please change it in your input files (`control.in` and `input.xml`) so that your server and your client always match!*

??? example "Note"
    If forget to add the 'use_pimd_wrapper UNIX:<address> <portnumber>' line in `control.in`, FHI-aims will not connect to i-pi and simply run a single point calculation.


## **Running i-PI with FHI-aims**

Let's run an i-PI+FHI-aims Molecular Dynamics simulation!

-   Open a terminal at the current directory and launch i-PI by typing

        i-pi input.xml

    At this point i-PI should start and parse the input file. At the bottom of the output on the screen it should say:
   
        Created unix socket with address EX1   
        @ForceField: Starting the polling thread main loop.   

    This means i-PI has started properly, has created the UNIX socket, and is waiting for the communications from the clients that take
    care of the force evaluations.

-   Now we can launch FHI-aims. Open a second terminal, either manually or by typing ctrl+shift+t, and enter the command

        aims.x

    (This command might vary depending on how you installed FHI-aims)

    Then FHI-aims should start and you will see some outputs.

-   Now switch to the terminal where i-PI is running, notice that i-PI has built the connection with FHI-aims with the following message,

        @SOCKET:   Client asked for connection from . Now hand-shaking.
        @SOCKET:   Handshaking was successful. Added to the client list.

    and started the Molecular Dynamics simulation. It should also print on screen information about the time taken for each MD step.

-   What we are going to do now is to kill FHI-aims (don't worry, you will not be sued for that). Simply switch to the terminal where FHI-aims is running and press `ctrl + c`. Now look at whether i-PI is still running. Notice that although the evolution of the MD is paused, i-PI itself does not die but instead continues to run and
    waits for a new client to take over. Now start FHI-aims again by typing:

        aims.x

    What happens to i-PI now?

??? note "Answer"
    i-PI restarts the dynamics and continues printing the corresponding steps on the original output.  

-   What if one stops i-PI? Trigger a soft exit of i-PI by typing `ctrl + c` at the terminal where it is running, or create an empty file named EXIT in the folder where i-PI is running (you can use the bash command `touch EXIT`). Watch how i-PI responds, and how FHI-aims reacts. Think about what are the advantages of a clean exit when a MD program stops unexpectedly.

??? note "Answer"
    When i-PI is stopped the client-server connection dies, i-PI stops at the subsequent step with a "SOFT EXIT" procedure, while FHI-aims does not run a new SCF cycle. Advantages of such an approach include saving the output files for the future, creating a RESTART file that can be used to rerun/continue the simulation from the current state.


-   Take a look at all the output files written by i-PI. You should have the file `ex1.out` that describes the system properties, `ex1.pos.xyz` that records the atomic trajectories, and `RESTART` that contains all the information to restart the simulation.

-   Last (and tricky) question: We are dealing with a charged system. Can you find where the total charge in the `input.xml` file is specified? Why?

??? note "Answer"
    The charge is not specified in `input.xml`. The charge (+1 for the zundel cation) is carried by the lack of an electron, and therefore needs to be specified in the electronic structure code (it is in `control.in`).  i-PI is concerned with the dynamics of the nuclei which are given by the forces calculated by FHI-aims in this case!



