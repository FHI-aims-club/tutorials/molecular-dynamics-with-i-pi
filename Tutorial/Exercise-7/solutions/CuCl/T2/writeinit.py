import numpy as np
from ase.io.aims import read_aims
atoms = read_aims('geometry.in')
ini = open('init.xyz','w')
posa = atoms.get_positions()
syms = atoms.get_chemical_symbols()
ini.write("%d\n" % len(syms))
ini.write("#2x2x2 cub supercell\n")
for i, a in enumerate(posa):
    ini.write("%s %.12f %.12f %.12f\n" % (syms[i],a[0],a[1],a[2]))
