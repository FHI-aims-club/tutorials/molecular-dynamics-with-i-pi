import numpy as np
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy.stats import norm

def Plot(xax,yax,xstart, xstop, Titel, Name, show = False, labelx = None, labely = None):
    plt.plot(xax,yax,linewidth=1)
    plt.xticks(fontsize= 15)
    plt.xlim(xstart, xstop)
    plt.yticks(fontsize= 15)
    if labelx != None:
        plt.xlabel(labelx,fontsize= 15)
    if labely != None:
        plt.ylabel(labely,fontsize= 15)
    plt.title(Titel,fontsize= 15)
    plt.savefig(Name, bbox_inches = 'tight')
    if show:
        plt.show()
    else:
        plt.clf()

def PlotHist(data,xstart, xstop, Titel, Name, bins = 30, show = False, labelx = None):
    (mu, sigma) = norm.fit(data)
    n, bins, patches = plt.hist(data, 30, density=True, facecolor='blue', alpha=0.75,histtype='step',linewidth=2)
    y = norm.pdf( bins, mu, sigma)
    l = plt.plot(bins, y, 'r--', linewidth=2)
    plt.xticks(fontsize= 15) 
    plt.xlim(xstart, xstop)
    plt.yticks(color='w')
    plt.grid(True)
    if labelx != None:
        plt.xlabel(labelx,fontsize= 15) 
    plt.title(Titel,fontsize= 15) 
    plt.savefig(Name, bbox_inches = 'tight')
    if show:
        plt.show()
    else:
        plt.clf()
stin = input("The maximum time to plot the results (in femtoseconds)")
stin = int(stin)
trin = input("Enter the paths to the directories of the trajectories you want to analyze, separated by spaces")
trajs = trin.split()
BtoA = 0.529177
atmtobar = 2.9421912*1e8
atmtoK = 1.0/3.1668152e-06
pmd = np.zeros(5001)
vol = np.zeros(5001)
T = np.zeros(5001)
con = np.zeros(5001)
pmt = []
pmtr = []
volt = []
voltr = []
timesteps = []
for i, t in enumerate(trajs):
    os.chdir('%s' %t)
    print(np.shape(np.loadtxt('simulation.out').T))
    step, time, conserved, potential, kinetic_cv, pressure_cv, volume, temperature, kinetic_md, pressure_md = np.loadtxt('simulation.out').T
    vol += volume
    volt.append(np.mean(volume[2000:]))
    pmd += pressure_md
    for vv in volume:
        voltr.append(vv)
    for pp in pressure_md:
        pmtr.append(pp)
    pmt.append(np.mean(pressure_md[2000:]))
    os.chdir("../")
    Plot(2*step, temperature*atmtoK, 0, 2*len(temperature), "Temperature %s" % t, "Temp%s.png" %t, labelx = "Time (fs)", labely = "Temperature (K)")
    Plot(2*step, temperature*atmtoK, 0, stin, "Temperature %s" % t, "Temp%dfs%s.png" % (stin,t), labelx = "Time (fs)", labely = "Temperature (K)")
    Plot(2*step, pressure_md*atmtobar/1000, 0, 2*len(pressure_md), "Pressure %s" % t, "Press%s.png" %t, labelx = "Time (fs)", labely = "Pressure (kbar)")
    Plot(2*step, pressure_md*atmtobar/1000, 0, stin, "Pressure %s" % t, "Press%dfs%s.png" % (stin,t), labelx = "Time (fs)", labely = "Pressure (kbar)")
    Plot(2*step, volume*BtoA**3/32, 0, stin, "Volume %s" % t, "Vol%dfs%s.png" % (stin,t), labelx = "Time (fs)", labely = "Volume ($\AA$)")
pmt = np.array(pmt)
pmtr = np.array(pmtr)
voltr = np.array(voltr)
volt = np.array(volt)
timesteps = np.zeros(len(vol))
for Ts in range(len(vol)):
    timesteps[Ts] += 2*Ts
vol /= float(len(trajs))
vol*=BtoA**3
vol /= 32.0 # Calculating volume of the primitive unitcell
voltr *= BtoA**3
voltr /= 32.0
pmd*=atmtobar
pmd/=float(len(trajs))
volt*=BtoA
volt /= 32.0
pmtr*=atmtobar
pmd /= 1000
print(np.mean(vol[2000:]),"+- ",np.std(volt), " (Ang^3)")
print(np.mean(pmd[2000:]),"+- ", np.std(pmt), " (bar)")
pmtr /= 1000
PlotHist(pmtr[2000:],-20,20, "CuCl Pressure", "CuClPressure.png", show = True, labelx = 'Pressure (kbar)')
PlotHist(voltr[2000:],voltr.min()-0.2,voltr.max()+0.2, "CuCl Volume", "CuClVolume.png", show = True, labelx = 'Volume ($\AA$)')
