# Preparations


## How to install FHI-aims

You will need a valid FHI-aims license for the next steps.
Information about how to obtain a license can be found here:
[https://fhi-aims.org/get-the-code](https://fhi-aims.org/get-the-code)

1. We will use the `210716_2` release for this tutorial series.
[Please download it from here.](https://fhi-aims.org/get-the-code-menu/downloads)
2. Extract the tarball with:
  ```
  tar -xzf fhi-aims.210716_2.tgz
  ```
!!! important "A note:"
    The tutorial should work with more recent versions of FHI-aims, at least until October 2024. Only some default settings have changed, which may impact some numerical results.

3. Follow the instructions on this page (you will need access to the FHI-aims GitLab):
[https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial](https://aims-git.rz-berlin.mpg.de/aims/FHIaims/-/wikis/CMake%20Tutorial),
or, alternatively, [download and read the manual](https://aimsclub.fhi-berlin.mpg.de/club_downloads.php).

## How to install i-PI

The easiest way to get i-PI is using `pip` package manager:

```
    pip install i-pi
```

A full manual of the i-PI program with a description of all flags is found
in this [link](https://ipi-code.org/i-pi/).
Find much more information about the i-PI code in [its webpage](https://ipi-code.org/).

Another, more flexible way to get i-PI is to clone its Git repository. Thus you obtain more
control over the source code, which is not needed for this tutotial,
but may be useful in practice.   

0. Prerequisites: Python version `>=3.6` and Numpy
1. Clone the i-PI repository:

    ```
    git clone https://github.com/i-pi/i-pi.git
    ```

2. Source the environment settings file `env.sh` as:

    ```
    source env.sh
    ``` 

    It is useful to put this line in your `.bashrc` or another settings file
    if you always want to have i-PI available.

