# Lecture

<iframe width="560" height="315" src="https://www.youtube.com/embed/UWjX5xRD7e8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Lecture slides: 

[Mariana Rossi: Ab initio molecular dynamics](https://indico.fhi-berlin.mpg.de/event/112/contributions/667/attachments/253/768/Forces-and-MD-with-i-PI.pdf)
