 [1] [M. Ceriotti, J. More, D. E. Manolopoulos, Comput. Phys. Comm.
**185** 1019 (2013).](https://doi.org/10.1016/j.cpc.2013.10.027)  
 [2] R. P. Feynman, and A. R. Hibbs *Quantum Mechanics and Path Integrals*;
McGraw-Hill: New York, 1965.  
 [3] D. Frenkel and B. Smit, *Understanding Molecular Simulation:
from algorithms to applications*, 2<sup>nd</sup> ed., Academic Press 2002.  
 [4] [G. Bussi, D. Donadio, and M. Parrinello, J. Chem.
Phys. **126**, 014101 (2007).](http://dx.doi.org/10.1063/1.2408420)  
 [5] [H.C. Andersen, J. Chem. Phys. **72**, 2384
(1980).](http://dx.doi.org/10.1063/1.439486)  
 [6] [H. J. C. Berendsen et al., J. Chem. Phys. **81**, 3684
(1984).](http://dx.doi.org/10.1063/1.448118)  
 [7] M. E. Tuckerman, *Statistical Mechanics: Theory and Molecular
Simulation*; Oxford University Press 2010.  
 [8] [M. Ceriotti, G. Bussi, and M. Parrinello, J. Chem. Theory Comput. **6**, 1170
(2010).](http://pubs.acs.org/doi/abs/10.1021/ct900563s)  
 [9] [S. Nosé, J. Chem. Phys. **81**, 511 (1984).](http://dx.doi.org/10.1063/1.447334)  
[10] [W.G. Hoover, Phys Rev. A **31**, 1695 (1985).](https://doi.org/10.1103/PhysRevA.31.1695)  
[11] [G. Martyna, M. Klein, and M. Tuckerman, J. Chem. Phys.**97** 2635 (1992).](https://doi.org/10.1063/1.463940)  
[12] D. McQuarrie, *Statistical Mechanics*, University Science Books, 1st. ed., (2000).  
[13] [X. Huang, B. J. Braams, and J. M. Bowman, J. Chem. Phys. **122**, 044308 (2005).](http://aip.scitation.org/doi/10.1063/1.1927529)  
[14] [M. Rossi, W. Fang, and A. Michaelides, J. Phys. Chem. Lett. **6**, 4233-4238 (2015).](https://pubs.acs.org/doi/pdf/10.1021/acs.jpclett.5b01899)  
[15] [G. Bussi, T. Zykova-Timan, M. Parrinello, J. Chem. Phys.**130**, 074101 (2009).](https://doi.org/10.1063/1.3073889)  
[16] [M. P. Allen and D. J. Tildesley, Computer Simulation of Liquids, Oxford University Press, Oxford, 1987]

