# The importance of step size

*Timing: $\sim$ 50 minutes per substance*

## **Introduction**

!!! note inline end ""
    A deuterium atom has one proton and one neutron in its nucleus, thus about twice as heavy as hydrogen.

Here, we will investigate the effects of the time step size for the integration of the equations of motion in a microcanonical molecular
dynamics simulation. For a better illustration, we will not only consider the H<sub>5</sub>O<sub>2</sub><sup>+</sup> molecule, but also its heavier, deuterated
counterpart D<sub>5</sub>O<sub>2</sub><sup>+</sup>.

Checkpoint files for both H<sub>5</sub>O<sub>2</sub><sup>+</sup> and D<sub>5</sub>O<sub>2</sub><sup>+</sup> from previous simulations where the molecules were thermalized are provided in the
[`exercise`](https://gitlab.com/FHI-aims-club/tutorials/molecular-dynamics-with-i-pi/-/tree/main/Tutorial/Exercise-3/exercise/) folder.
Use the `control.in` with default self-consistency convergence criteria and the `geometry.in` files from the previous exercise.
Both H<sub>5</sub>O<sub>2</sub><sup>+</sup> and D<sub>5</sub>O<sub>2</sub><sup>+</sup> have the exact same electronic structure.
Under the Born-Oppenheimer approximation, changing the nuclear mass (as long as the nuclear charge is not changed), cannot change the electronic ground state. 
Therefore, one can use the same FHI-aims input files for both molecules.
What will matter, though, is that in the evolution of the nuclei through Newton's equation of motion, the mass of the atom will enter in the propagation of the equations of motion.
Therefore we will use different masses (corresponding to hydrogen and deuterion) in the checkpoint file that i-PI is reading at its initialization.

!!! note "Find the masses"
    If you wish to have a look at where this is specified, open the checkpoint files and look for the block named `<m shape=... >`. (The units are atomic mass units.)

An `input.xml` file is also provided in the directory. Please read and complete it in the appropriate way for each of the following tasks.

In order to speed up the exercise in the live tutorials, you will be asked to simulate either H<sub>5</sub>O<sub>2</sub><sup>+</sup> or D<sub>5</sub>O<sub>2</sub><sup>+</sup>.

## **Instructions to set up the calculations**

-   Copy the "default" `control.in` and the `geometry.in` files from the last exercise.

    Do not add any flags that we do not mention! They are not needed and might hinder the performance of the calculation.

    ??? example inline end "Hint"
        This should be 800 steps

-   Modify the `input.xml` file to run a 0.40 ps MD run in the microcanonical ensemble, using a 0.0005 ps ($\Delta t$ = 0.5 fs) time step.

-   Complete all the missing fields ('xxx') in the `input.xml` file.

-   Run the simulation.

-   When it is done, plot the total energy vs. the simulation time.

-   Increase the time step to 0.001 ps and change the number of steps accordingly to have again a total of 0.40 ps simulation time. Take
    the necessary precautions to not overwrite the files and run the simulation again.

-   Increase the time step to 0.002 ps and change the number of steps accordingly to have again a total of 0.40 ps simulation time. Take
    the necessary precautions to not overwrite the files and run the simulation again. 
    
!!! note "FHI-aims aborts"
    If FHI-aims aborts the run, you can create a file named EXIT in the folder where i-PI is running to stop it in a clean
    way by typing `touch EXIT` in your terminal window.

## **Analysis of the MD runs**

-   Plot the total energy vs. simulation time for $\Delta t$ = 0.001 ps, $\Delta t$ = 0.002 ps, and $\Delta t$ = 0.0005 ps. If you're simulating D<sub>5</sub>O<sub>2</sub><sup>+</sup> please try also $\Delta t$ = 0.0033 ps.
    You can find a python plotting script in the solutions directory.
    Run it by the command

        python plot_Energy.py <path-file-1> <path-file-2> <path-file-3>

    How do the energy fluctuations develop? What is happening for the $\Delta t$ = 0.002 ps (or for D<sub>5</sub>O<sub>2</sub><sup>+</sup> $\Delta t$ = 0.0033 ps) run?

!!! note "Answer"
    From a practical point of view, a larger time step is desirable, since it allows to assess longer trajectories for a given computational time.
    Notice, however, that the $\Delta t$ = 0.002 ps simulation diverges for H<sub>5</sub>O<sub>2</sub><sup>+</sup>. 

    <center>
        <img src='/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/H5O2/fs_0.5-fs_1.0-fs_2.0-energy-fluct.png' height="75%" width=75% title='energy-fluctuations-H5O2' alt='energy-fluctuations-H5O2'>
        <img src='/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/D5O2/fs_0.5-fs_1.0-fs_2.0-fs_3.3-energy-fluct.png' height="75%" width=75% title='energy-fluctuations-D5O2' alt='energy-fluctuations-D5O2'>
    </center>

What do you see upon inspecting the dynamics of the molecule by opening `ex3.pos.xyz` in VMD (or Jmol, or Molden, or whichever program you prefer)?

!!! note "Answer"
    In fact, the molecule dissociates. 
    <center>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/H5O2/fs_0.5/05fs.gif" width="32% "height="50%" alt="step = 0.5 fs" title="step = 0.5 fs"/>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/H5O2/fs_1.0/1fs.gif" width="32% "height="50%" alt="step = 1.0 fs" title="step = 1.0 fs"/>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/H5O2/fs_2.0/2fs.gif" width="32% "height="50%" alt="step = 2.0 fs" title="step = 2.0 fs"/>
    </center>
The reason for the dissociation is that the integrator is unable to deal with these "big" time steps. This integrator uses a simple Verlet algorithm
[^frenkel], where the error in the integrator goes with $\Delta t^4$. 

If you are simulating D<sub>5</sub>O<sub>2</sub><sup>+</sup>, the $\Delta t$ = 0.002 ps simulation does not diverge, although the energy fluctuations become very large. 
You can also look at the dynamics of this molecule in jmol or VMD.

!!! note "Answer"

    <center>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/D5O2/fs_0.5/05fs.gif" width="23% "height="50%" alt="step = 0.5 fs" title="step = 0.5 fs"/>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/D5O2/fs_1.0/1fs.gif" width="23% "height="50%" alt="step = 1.0 fs" title="step = 1.0 fs"/>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/D5O2/fs_2.0/2fs.gif" width="23% "height="50%" alt="step = 2.0 fs" title="step = 2.0 fs"/>
        <img src="/tutorials/molecular-dynamics-with-i-pi/Exercise-3/solutions/D5O2/fs_3.3/33fs.gif" width="23% "height="50%" alt="step = 3.3 fs" title="step = 3.3 fs"/>
    </center>

Although such large energy fluctuations would already not be accurate enough for a production run with this molecule, the fact that it does
not explode illustrates an important point: the largest $\Delta t$ that can be used in a particular integration algorithm depends on the highest
vibrational frequency of the system. Since the D atoms, being heavier, have a larger vibrational period (Do you understand why is this? Think about a harmonic oscillator),
the $∆t$ that can be used for the integrator can also be larger.

[^frenkel]: D. Frenkel and B. Smit, *Understanding Molecular Simulation: from algorithms to applications*, 2<sup>nd</sup> ed., Academic Press 2002.
