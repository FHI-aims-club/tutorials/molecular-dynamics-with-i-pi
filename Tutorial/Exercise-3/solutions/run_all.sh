 for i in  D5O2_fs_0.5 D5O2_fs_1.0 H5O2_fs_0.5 H5O2_fs_1.0 D5O2_fs_2.0 D5O2_fs_3.3 H5O2_fs_2.0
 do 
 pwd
 cd ${i}
    rm -f EXIT
    i-pi input.xml >output_i-PI &
    sleep 3 
    mpirun -np 6 ~/Codes/bin/aims.x >output_FHI-aims
    touch EXIT
 cd ..
 done
