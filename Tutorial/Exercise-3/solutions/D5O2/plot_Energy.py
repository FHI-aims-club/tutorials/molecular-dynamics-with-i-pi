# !/usr/env/bin python

from matplotlib import pyplot as plt
import numpy as np
import sys

filenames = sys.argv[1:]

for filename in filenames:
    print(filename)

    data = np.loadtxt(filename, usecols=(1,2))
    plt.plot(data[:,0]*1000, data[:,1], '-', label=r"$\Delta t = $" + str(filename.split('/')[0].split('_')[1]) + ' fs')


# Format 
plt.xlabel(r'time / fs',fontsize=20)
plt.ylabel(r'conserved energy / Ha',fontsize=20)
plt.xticks(fontsize = 13)
plt.yticks(fontsize = 13)
plt.tight_layout()
plt.ticklabel_format(axis='y', style='sci')
plt.xlim([0.,400])
plt.ylim([-152.1222,-152.12])
plt.legend(loc='best')
plt.tight_layout()
plt.title(r"$D_5O_2^+$")
filename_out=''
for filename in filenames:
   filename_out+='{}-'.format(filename.split('/')[0])
filename_out+='energy-fluct.png'
plt.savefig(filename_out, dpi=300)



plt.show()


