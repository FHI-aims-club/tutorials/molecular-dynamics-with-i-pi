#!/bin/bash
# This script takes basename of the output file, e.g. lgv2 for lgv2.out,
# and plots cumulative average temperature by species,
# assuming that you have already calculated it useng the other scripts.

gnuplot << EOF
# set term png size 800,300 font "Sans, 14"
# set output "cumul_avg.$1.png"
 set term qt size 800,300 font "Sans, 14"
 set grid

 set xlabel 'time (ps)'
 set ylabel 'E_{kin} per atom (K)'

# set yrange [250:600]
 set logscale x

 p 'cumul_avg_0_kinetic_md(H)_${1}.out' u (\$0/1000.):(\$1/5) w l lw 2 t '${1} H',\
   'cumul_avg_0_kinetic_md(O)_${1}.out' u (\$0/1000.):(\$1/2) w l lw 2 t 'O'

 pause mouse close
EOF
