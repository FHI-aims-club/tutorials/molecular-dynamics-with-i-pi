from __future__ import print_function
import sys

print()

# Check syntax
if len(sys.argv) != 3:
    print("")
    print("You have provided an incorrect syntax.")
    print("The correct syntax is:")
    print('python cumul_avg.py "filename" "skip"')
    print("Example:  python get_properties.py ipi-output 100")
    print()
    sys.exit()
else:
    input_name = sys.argv[1]
    skip = sys.argv[2]

# Try to open file
try:
    file0 = open(input_name, "r")
except:
    print("We can't find the file '{}'.".format(input_name))
    print("")
    sys.exit()

# skip the requested lines
for i in range(int(skip)):
    line = file0.readline()
print("We have skipped the first {} lines".format(skip))

output = open("cumul_avg_" + skip + "_" + input_name, "w")
tot = 0.0
counter = 0
while True:
    line = file0.readline()
    if not line:
        break
    counter += 1
    tot += float(line.split()[0])
    avg = tot / counter
    output.write(str(avg) + "\n")

output.close()
