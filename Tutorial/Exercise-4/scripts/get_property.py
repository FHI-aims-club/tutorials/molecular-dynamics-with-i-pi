from __future__ import print_function
import sys
print()


#Check syntax
if len(sys.argv) != 3:
    print('')
    print('You have provided an incorrect syntax.')
    print('The correct syntax is:')
    print('python get_properties.py "property" "filename"')
    print('Example:  python get_properties.py potential ipi-output ')
    print()
    sys.exit()
else:
   input_name = sys.argv[2]
   datatype = sys.argv[1]


#Try to open file
try:
   input_file = open(input_name,'r')
except:
   print("We can't find the file '{}'.".format(input_name))
   print('')
   sys.exit()

#Get all the available propeties
properties = list()
while True:
    line = input_file.readline().split()
    if not line:
       print('We have a problem here. Please check your inputfile')
       sys.exit()
    if line[0] == '#':
       if '{' in line[4]:
        properties.append(line[4].split('{')[0])
       else:
        properties.append(line[4])
    else: 
       break


#Check if the requested property is available
if datatype not in properties:
    print('"{}" is not a valid property'.format(datatype))
    print('We have found only:')
    print('{}'.format(properties))   
    print()
    sys.exit()
else:
   index_prop = properties.index(datatype)
   index_time = properties.index('time')
   index_step = properties.index('step')
   outname = datatype + '_' + input_name
   outfile = open(outname, 'w')


# Go through the whole file, line by line, and print the chosen property against time
outfile.write(line[index_prop]+'\n')
while True:
    line = input_file.readline()
    if not line:
        break
    outfile.write(line.split()[index_prop]+'\n')

outfile.close()
print('Please check {}'.format(outname))
print()
