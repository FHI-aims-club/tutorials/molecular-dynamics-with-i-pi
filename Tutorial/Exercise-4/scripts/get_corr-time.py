# 1/usr/bin/env python3

import sys
import os
from scipy import signal
import numpy as np

print()

threshold = 0.02
# Check syntax
if len(sys.argv) != 3:
    print(
        "You have provided an incorrect syntax.\n"
        "The correct syntax is:\n"
        "python get_corr-time.py 'filename' 'skip'\n"
        "Example:  python get_corr-time.py ipi-output 500\n"
    )
    print()
    sys.exit(1)
else:
    input_name = sys.argv[1]
    skip = sys.argv[2]

# Try to open file
exists = os.path.isfile(input_name)
if not exists:
    print("We can't find the file '{}'.".format(input_name))
    print("")
    sys.exit()
else:
    data = np.loadtxt(input_name)[int(skip) :]
    if data.ndim != 1:
        print("Error: single-column data expected.")
        sys.exit(1)
    print("We have skipped the first {} lines".format(skip))
    print("We have {} data points".format(data.size))

# Compute normalized correlation function
data -= np.mean(data)
data = data.flatten()

#data2 = np.concatenate((data, np.zeros(data.size)))
#acf = signal.correlate(data2, data, mode="valid")
acf = signal.correlate(data, data, mode="full")[data.size:]
# Normalize ACF so that ACF(t=0) = 1
acf /= acf[0]

tau = np.where(acf.flatten() < threshold)[0][0]

# Print
print("The correlation time is approx. {} ".format(tau))
print()
print(
    "You can check the autocorrelation function in the file: {}".format(
        "corr" + "_" + input_name
    )
)
print()
np.savetxt("corr_" + input_name, acf)
