#!/usr/bin/env python3

import sys
import os 
import numpy as np
print()

#Check syntax
if len(sys.argv) != 3:
    print("You have provided incorrect arguments.\n"
          "The correct syntax is:\n"
          "python get-average.py 'filename' 'skip'\n"
          "Example:  python3 get_average.py ipi-output 500\n")
    sys.exit()
else:
    input_name = sys.argv[1]
    skip = sys.argv[2]

#Try to open file
exists = os.path.isfile(input_name)
if not exists:
    print("We can't find the file '{}'.\n".format(input_name))
    sys.exit()
else:
   data = np.loadtxt(input_name)[int(skip):]
   print('We have skipped the first {} lines'.format(skip))
   print('We have {} data points'.format(data.size)) 

mean  = np.mean(data)
std   = np.std(data)

print('mean = {}'.format(mean))
print('std = {}'.format(std))
print()
