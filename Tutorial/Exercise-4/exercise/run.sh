#!/bin/bash

export OMP_NUM_THREADS=1
. ~/soft/i-pi-sabia/env.sh

python3 ~/bin/i-pi input.xml | tee log.ipi &

sleep 1
echo '2...'
sleep 1
echo '1...'
sleep 1

mpirun -np 2 ~/bin/aims.x | tee aims.out
