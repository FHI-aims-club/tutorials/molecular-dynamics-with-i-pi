
<!--- # Hands-On DFT and Beyond: High-throughput screening and big-data analytics, towards exascale computational materials science # --->

# *Ab initio* Molecular Dynamics with FHI-aims and i-PI #

**Prepared by Karen Fidanyan, Eszter Sarolta Pós, H. Henrik Kowalski,  and Mariana Rossi (based on previous years tutorials with contributions from: Yair Litman, Marcin Krynski, Nathaniel Raimbault, Luca Ghiringhelli)**

**Revisions: Yair Litman (Sep-2024)**

We aim to introduce techniques for solving the classical equations of motion
for a system of interacting particles. The strategy we adopt in this tutorial is known as molecular dynamics (MD).
In a MD framework, a system of particles is *propagated* in time, starting from given initial positions
and momenta, by numerically integrating  Newton's equations of motion.
This technique allow us to sample the potential energy surface of a system at constant energy
(microcanonical ensemble) and at constant temperature (canonical ensemble).
Provided the [*ergodic hypothesis*](https://en.wikipedia.org/wiki/Ergodic_hypothesis)
holds, time-averaged parameters (or properties) can be related
to thermodynamic averages over the corresponding statistical ensemble.

We will employ Born-Oppenheimer MD, that is, the instantaneous position of the nuclei
only define the potential at which the electronic-structure problem is solved, and this solution
defines the potential in which the nuclei move (Born-Oppenheimer approximation).
Unless specified otherwise, the nuclei are treated as classical particles.

In this tutorial, the electronic structure problem is solved via density-functional theory (DFT)
using the FHI-aims code and the nuclear problem is managed by the i-PI code.
i-PI works through a client-server paradigm, where the *ab initio* code,
in this case FHI-aims, is the client that provides the calculation of interatomic forces,
and i-PI is the server that provides the evolution of the equations of motion that sample
the desired ensemble. 

## The Objective

At the end of this tutorial, you will have learned

* the basics of driving FHI-aims with i-PI
* the basics of running a MD simulation in the microcanical, canonical, and isothermal–isobaric ensembles.
* how to perform harmonic vibrational analysis and compute anharmonic vibrational free energies

## Location of the tutorial material

All files related to the tutorial, including solutions, can be found at
<https://gitlab.com/FHI-aims-club/tutorials/molecular-dynamics-with-i-pi>.



## Prerequisites

* A sufficient understanding about the basics of running FHI-aims is required.
Please review our tutorial [Basics of Running FHI-aims](https://fhi-aims-club.gitlab.io/tutorials/basics-of-running-fhi-aims/)
if you have not yet done so and/or if you are unfamiliar with the code.

* A sufficiently powerful computer. For this tutorial a laptop with at least four (physical cores) should be sufficient. 
Moreover, timing informations available at the front of each exercise denote approximate runtimes of each task on a 4-core laptop.
These include time spent on setting up input files and following instructions step by step, too.

* The FHI-aims and i-PI code distribution must be present on this computer.

* A Python installation, version `>=3.6`, and Numpy.

* All the necessary theoretical background about statistical mechanics and *ab initio*
molecular dynamics needed for a good understanding of this tutorial can be found
in the 2019's lecture of **Luca Ghiringhelli** (
[Ab initio statistical mechanics and molecular dynamics](https://th.fhi-berlin.mpg.de/meetings/DFT-workshop-2019/uploads/Meeting/Luca.pdf)) and 2021's lecture by **Mariana Rossi** ([Ab initio statistical
mechanics and molecular dynamics](https://indico.fhi-berlin.mpg.de/event/112/contributions/663/attachments/251/755/Rossi.pdf)).



## Summary of the tutorial

The tutorial consists of seven exercises. The first six of them are based on a paradigmatic system,
namely, the gas-phase (isolated) H$_5$O$_2$$^+$, the Zundel cation while the last one utilizes a periodic system of copper-chloride.

<center>
    <img src="/tutorials/molecular-dynamics-with-i-pi/H5O2.png" title="The studied Zundel cation" alt="The studied Zundel cation" width="67%" height="67%">
<figcaption>Zundel cation</figcaption>

</center>

If you have not set up FHI-aims and i-PI on your machine, please start with the [**Preparations**](/tutorials/molecular-dynamics-with-i-pi/preparations) section. 

[**Part 1**](/tutorials/molecular-dynamics-with-i-pi/Exercise-1/exercise-1) is a quick, introductory exercise that presents the machinery
we will use through the tutorial. In particular, you will learn how to setup
the i-PI and FHI-aims inputs.

[**Part 2**](/tutorials/molecular-dynamics-with-i-pi/Exercise-2/exercise-2) and [**Part 3**](/tutorials/molecular-dynamics-with-i-pi/Exercise-3/exercise-3) pose the problem of choosing the right settings for obtaining a stable and reliable molecular dynamics trajectory.
In particular, we will investigate the effect of the self-consistent cycle and force convergence thresholds as well as the influence of the size of the time step.

[**Part 4**](/tutorials/molecular-dynamics-with-i-pi/Exercise-4/exercise-4) deals with the choice of a thermostat for simulating the canonical ensemble,
i.e., the contact between your system of interest and a thermal reservoir.
In this tutorial we will focus on the so-called stochastic thermostats.
Other implementations, for example based on extended Lagrangians exist as well, and will be briefly explained in the exercise.
Also, we will introduce the concept of statistical uncertainty and learn how to estimate it.

[**Part 5**](/tutorials/molecular-dynamics-with-i-pi/Exercise-5/exercise-5) shows how to perform harmonic vibrational analysis  with i-PI.

In [**Part 6**](/tutorials/molecular-dynamics-with-i-pi/Exercise-6/exercise-6), we focus on anharmonic free energy calculation. More specifically, we look at the difference between the harmonic approximation,
obtained in [**Part 5**], and the total (anharmonic) free energy. In this case, we will use the technique called thermodynamic integration.

[**Part 7**](/tutorials/molecular-dynamics-with-i-pi/Exercise-7/exercise-7) introduces how to set up constant pressure dynamics runs using the barostat functionality of
i-PI combined with FHI-aims. The example showcases dynamics on a periodic system of copper-chloride.  
